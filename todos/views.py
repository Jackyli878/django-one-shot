from django.shortcuts import render, get_object_or_404, redirect
from todos.forms import TodoListForm, TodoItemForm
from todos.models import TodoList, TodoItem

# Create your views here.


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save(False)
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_details", id=id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "list": list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_details", id=id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "list": item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)


def delete_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_list(request):
    todo_list_view = TodoList.objects.all()
    context = {
        "todo_objects": todo_list_view,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    print(f"TodoList ID:{id}")
    todos = get_object_or_404(TodoList, id=id)
    print(f"List Name: {todos.name}")
    context = {
        "todos_details": todos,
    }
    return render(request, "todos/detail.html", context)
